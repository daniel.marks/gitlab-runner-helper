ARG RUNNER_REVISION

FROM gitlab/gitlab-runner-helper:x86_64-$RUNNER_REVISION
# when updating the tag, also update gitlab ci and config.toml

#RUN sed -i -e 's/umask 0000/umask 0022/' /usr/bin/gitlab-runner-build

RUN addgroup -g 59417 -S nonroot && \
    adduser -u 59417 -S nonroot -G nonroot
WORKDIR /home/nonroot

USER 59417:59417
